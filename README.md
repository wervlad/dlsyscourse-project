# Final Project for 10-414/714: Deep Learning Systems (online class)

This project is a library for deep learning tasks similar to PyTorch, but much simpler. It includes a simple backend library for working with n-dimensional arrays and performing mathematical operations on them. The library is written in C++ and CUDA and supports both CPU and GPU. On top of it, GRU, CNN, RNN, LSTM, SGD, Adam, normalization, dropout algorithms, and other necessary underlying operations are implemented from scratch. The project was completed as part of the course 10-414/714: Deep Learning Systems at Carnegie Mellon University.

For more details please watch the [video presentation](https://youtu.be/0ZCG0Lg8W2o) and read the [final report](final_report.ipynb)
